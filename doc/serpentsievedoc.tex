% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={blue!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

\def\X#1{$#1$ &\tt\string#1}




%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The "real"  content comes below...

\title{Serpent Sieve Documentation}
\author{Bill Wangard}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

%\newenvironment{boxeq}
 % \begin{tcolorbox}
 % \begin{equation}
  %{}
 % \end{equation}
%  \end{tcolorbox}
%}

   
    \newenvironment{boxeq}%
{\begin{tcolorbox}[colback=blue!15!white,
colframe=blue!75!black]\begin{equation}}%
{\end{equation}\end{tcolorbox}}
    
    
       \newenvironment{boxeqarray}%
{\begin{tcolorbox}
[colback=blue!15!white,
colframe=blue!75!black]\begin{eqnarray}}
{\end{eqnarray}\end{tcolorbox}}
    
\begin{document}
\maketitle

\tableofcontents

\section{Mixture Composition Definitions and Conversion Formulae}

\subsection{Definitions}

The mixture of species in a given phase is assumed to consist of a $N$ components.  These could be individual elements
and/or various nuclides, since each nuclide has a different molecular weight.

The following table lists the definitions of various terms in this document:
\begin{table}[h!]
\centering
\begin{tabular}{||c l l||}
\hline
Variable & Units & Definition \\
\hline
$m$	& g & Total mass ] \\
$n$ & mol & Total number of moles \\
$W$ &  $\textrm{g}/\textrm{mol}$ & Total molecular weight \\
$\rho$ & $\textrm{g}/\textrm{cm}^3$ & Mass Density \\
$c$ &  $\textrm{mol}/\textrm{cm}^3$  & Molar Concentration \\
$\tilde{c}$ &  $\textrm{atoms}/\textrm{cm}^3$  & Atomic Concentration \\
$m_i$ & g & Mass of the $i^{th}$ component \\
$n_i$ & mol &  Moles of the $i^{th}$ component \\
$y_i$ & & Mass Fraction of the $i^{th}$ component \\
$x_i$ & & Mole Fraction of the $i^{th}$ component \\
$W_i$ & $\textrm{g}/\textrm{mol}$ & Molecular Weight of he $i^{th}$ component \\
$\rho_i$ & $\textrm{g}/\textrm{cm}^3$ & Mass Density of the $i^{th}$ component \\
$c_i$ & $\textrm{mol}/\textrm{cm}^3$ & Molar Concentration of the $i^{th}$ component \\
$\tilde{c}_i$ & $\textrm{atoms}/\textrm{cm}^3$ & Atomic Concentration of the $i^{th}$ component \\
$\mathcal{N_A}$ & $\textrm{1}/\textrm{mol}$ & $6.022 \times 10^{23} \; \textrm{1}/\textrm{mol}$ \\
\hline
\end{tabular}
\end{table}


\subsection{Evaluation of Principal Terms in the Bulk}

\subsubsection{Total Mass}
\begin{equation}
m = \sum_{i=1}^N m_i
\end{equation}

\subsubsection{Total Moles}
\begin{equation}
n = \sum_{i=1}^N n_i
\end{equation}

\subsubsection{Density}
\begin{equation}
\rho = \frac{m}{V}
\end{equation}

\subsubsection{Molar Concentration}
\begin{equation}
c = \frac{n}{V}
\end{equation}

\subsubsection{Atomic Concentration}
\begin{equation}
\tilde{c} = \mathcal{N_A} \frac{n}{V}
\end{equation}

\subsubsection{Molecular Weight}
\begin{equation}
W = \frac{m}{n} = \frac{\rho}{c}
\end{equation}


\subsection{Evaluation of Principal Terms by Component}

\subsubsection{Mass Fraction}
\begin{equation}
y_i = \frac{m_i}{m}
\end{equation}

\subsubsection{Mole Fraction}
\begin{equation}
x_i = \frac{n_i}{n}
\end{equation}

\subsubsection{Component Molecular Weight}
\begin{equation}
W_i = \frac{m_i}{n_i}
\end{equation}

\subsubsection{Component Mass Density}
\begin{equation}
\rho_i = \frac{m_i}{V}
\end{equation}

\subsubsection{Component Molar Concentration}
\begin{equation}
c_i = \frac{n_i}{V}
\end{equation}

\subsubsection{Component Atomic Concentration}
\begin{equation}
\tilde{c}_i =\mathcal{N_A} \frac{n_i}{V}
\end{equation}


\subsection{Conversion Formulae}

\subsubsection{Mixture Molecular Weight (from mole fractions)}
\begin{boxeqarray}
W &=&\frac{m}{n} \\
 &=& \frac{\sum m_i}{n} \\
 &=& \frac{\sum n_i W_i}{n} \\
\label{eq:mmw}
 &=& \sum {x_i W_i}
\end{boxeqarray}

\subsubsection{Mixture Molecular Weight (from mass fractions)}
\begin{boxeqarray}
W &=&\frac{m}{n} \\
 &=& \frac{m}{\sum n_i} \\
 &=& \frac{m}{\sum {m_i/W_i}} \\
 &=& \left({\sum {y_i / W_i}}\right)^{-1}\def\X#1{$#1$ &\tt\string#1}
\end{boxeqarray}

\subsubsection{Component Concentration to Mass Fraction}
\begin{equation}
y_i = \frac{c_i W_i}{\rho}
\end{equation}

\subsubsection{Component Density to Mass Fraction}
\begin{equation}
y_i = \frac{\rho_i}{\rho}
\end{equation}

\subsubsection{Mole Fraction to Mass Fraction}
\begin{boxeq}
\label{eq:x2y}
y_i = \frac{x_i W_i}{\sum_k x_k W_k}
\end{boxeq}

\subsubsection{Mass Fraction to Mole Fraction}
\begin{boxeq}
x_i = \frac{y_i / W_i}{\sum_k y_k / W_k}
\end{boxeq}


\section{Atomic-Level Composition from Molecular Composition}

Several nuclear codes require breakdown of a mixture in terms of all of the nuclides.  However, for chemical 
reaction computations, molecular composition is generally all that is needed.

We consider a mixture of $M$ species designated by a vector of names representing molecular species:
\begin{equation}
\sigma = \begin{bmatrix}
       \sigma_1  \\[0.3em]
       \sigma_2  \\[0.3em]
       \dots \\[0.3em]
       \sigma_M   
     \end{bmatrix}
\end{equation}

Next, we define the vector of $N$ atomic species that appear in our list of molecular species.
\begin{equation}
\alpha = \begin{bmatrix}
       \alpha_1  \\[0.3em]
       \alpha_2  \\[0.3em]
       \dots \\[0.3em]
       \alpha_N   
     \end{bmatrix}
\end{equation}

The matrix of stoichiometric coefficients, $C$, is populated  such that where the $C_{ij}$ is the stoichiometric coefficient
of atomic species $j$ in molecular species $i$.  It will have $M$ rows and $N$ columns.

\begin{equation}
C = \begin{bmatrix}
       c_{1,1} & \dots & c_{1,N}  \\[0.3em]
       \vdots &  \ddots & \vdots \\[0.3em]
       c_{M,1} & \dots & c_{M,N}    
     \end{bmatrix}
\end{equation}

The vector of mole fractions of the molecular species is given by $x_\sigma$, where the subscript is used to make it clear
that this vector is of the molecular species.

The  solution for the vector of mole fractions in the atomic space, $x_\alpha$  is given by the following transformation:
\begin{boxeq}
\label{eq:xalpha}
x_\alpha = \frac{C^T x_\sigma}{\sum_{k=1}^{N}{\left( C^T x_\sigma \right)}_k}
\end{boxeq}

\subsection{Calculation of Nuclidal Mole Fractions}
To split out the mole fractions amongst the nuclides (isotopes of atoms) we first generate the list of nuclides that are relevant.

Most of the time, nuclidal decomposition is done by mole fraction.   We define the nuclidal mole fraction of the $j^{\textrm th}$ 
nuclide of the $i^{\textrm th}$ atomic species as $x_i^j$.  And for each atomic species, there are a total of $K_i$ nuclides.

The rows of the nuclide matrix, $\nu$ are the nuclidal species, and the columns are the mole fractions of the associated atomic
element in proportion to the nuclide occurrence.  
\begin{boxeq}
  \nu = \begin{bmatrix}
       x_1^1 & 0 & \dots & 0  \\[0.3em]
       x_1^2 & 0 & \dots & 0  \\[0.3em]
       \vdots  & \vdots & \dots & \vdots  \\[0.3em]
       x_1^{K_1} & 0 &  \dots & 0  \\[0.3em]
       0 &  x_2^1 & \dots & 0  \\[0.3em]
       0 &  x_2^2 & \dots & 0  \\[0.3em]
      \vdots  & \vdots & \dots & \vdots  \\[0.3em]
       0 &  x_2^{K_2} & \dots & 0  \\[0.3em]
      \vdots  & \vdots & \dots & \vdots  \\[0.3em]
       0 & 0 & \dots &  x_N^1   \\[0.3em]
       0 & 0 & \dots &  x_N^2   \\[0.3em]
      \vdots  & \vdots & \dots & \vdots  \\[0.3em]
       0 & 0 & \dots &  x_N^{K_N}   \\[0.3em]
       \end{bmatrix}
\end{boxeq}


The nuclidal mole fraction vector is then computed as follows:
\begin{boxeq}
x_\nu = \nu x_\alpha
\end{boxeq}

\section{Normalization of Mass or Mole Fractions}
Normally, the mass fractions and mole fractions should sum to unity.  However, there are times in which machine precision and roundoff errors cause deviations
from this.  Normalization of these terms is useful from time to time.  Perhaps the easiest way to do this is as follows:


\begin{boxeq}
\label{eq:xnorm1}
x_{norm} = \frac{x}{\sum_k x_k}
\end{boxeq}


Another method possibly is to apply the following correction to each mole fraction component:
\begin{equation}
\label{eq:xnorm2}
x_{norm,i} = x_i + \frac{1}{N} \left( 1 - \sum_{k=1}^{N} x_k \right)
\end{equation}

The default method should be that given by Eq.~(\ref{eq:xnorm1}), however it may be useful to employ both methods.

\section{Converting the Nuclidal Mole Fraction Vector to Molecular Mole Fractions}
In this section, the equations are shown for converting the nuclidal mole fractions, $x_\nu$ 
to atomic mole fractions, $x_\alpha$.

First, we begin by assuming that the vector of nuclides may include species that have for all intents and purposes
disappeared, so that their mole fraction is zero for all nuclides of a particular element.  In such a case, our method
should avoid singularities in the computation.

The first thing to do is to add a trace amount of species to each component, so mathematically we avoid singularities.
\begin{boxeq}
  \label{eq:xnustar}
  x_\nu^* =    x_\nu + \epsilon \left\{ 1 \right\}
  \end{boxeq}
  where $\left\{ 1 \right\}$ is a vector of ones that is the same length as $x_\nu$, and $\epsilon$ is a very small number.
  

The nuclidal molar mass matrix, $W_\nu$, is given as the molecular masses of each atomic species (by row) spanning columns over each occuring nuclide;
\setcounter{MaxMatrixCols}{20}
 
\begin{boxeq}
W_\nu = \begin{bmatrix}
   \label{eq:Wnu}
    W_1^1 & W_1^2 & \dots & W_1^{K_1} & 0  &  \dots  &  &  &  &  &  &  &  &   \\[0.3em]
     &    & \dots & 0 & W_2^1 & W_2^2 & \dots & W_2^{K_2} & 0 & \dots &  &  &  &  \\[0.3em]
     &   &  &  &   &  & \dots & 0 & W_3^1 & W_3^2 & \dots & W_3^{K_3} & 0 & \dots \\[0.3em]
   \end{bmatrix}
\end{boxeq}

We define a summation operator, $S$, matrix is defined as 1 where a nuclide is present for the given atomic element zero elsewhere.
For the above matrix, we have the following:

\begin{boxeq}
S = \begin{bmatrix}
    \label{eq:summation}
    1 & 1 & \dots & 1 & 0  &  \dots  &  &  &  &  &  &  &  &   \\[0.3em]
     &    & \dots & 0 & 1 & 1 & \dots & 1 & 0 & \dots &  &  &  &  \\[0.3em]
     &   &  &  &   &  & \dots & 0 & 1 & 1 & \dots & 1 & 0 & \dots \\[0.3em]
   \end{bmatrix}
\end{boxeq}


The atomic mole fraction vector is calculated by summing up the mole fractions of each nuclide as follows:

\begin{boxeq}
\label{eq:xalphafromxnu}
x_{\alpha} = S x_\nu^*
\end{boxeq}

Additionally, we have to compute the new atomic molecular weights based upon the nuclide ratios

To calculate the vector of atomic molecular weights, we use the following formula:
\begin{boxeq}
W_\alpha ={\left[ \textrm{diag} \left( x_\alpha \right) \right]}^{-1} W_\nu x_\nu^*
\end{boxeq}
 
where $\textrm{diag}$ is the diagonal matrix formed from the elements of $W_\nu^{norm} x_\nu$:

\begin{equation}
{\left[ \textrm{diag} \left( x_\alpha \right) \right]}^{-1} = \begin{bmatrix}
   1/x_{\alpha_1} & 0 & 0 \\[0.3em]
   0 & 1/x_{\alpha_2} & 0 \\[0.3em]
   0 & 0 & \ddots 
   \end{bmatrix}
   \end{equation}
   
   The normalization process to calculate $x_\nu^*$ will prevent numerical singularities from forming in the
   matrix.




\subsection{Example}
As an example, we consider a mixture of sodium and uranium trichloride with mole fractions of 25\% and 75\%, respectively.
 Our molecular species vector, $\sigma$
is given by
\begin{equation}
\sigma = \begin{bmatrix}
\textrm{Na}\textrm{Cl}  \\[0.3em]
\textrm{U}\textrm{Cl}_3
\end{bmatrix}
\end{equation}

Parsing our list of species, we note that there are 3 atomic species: sodium (Na), chlorine (Cl), and uranium (U).  The vector of
atomic species may be given by
\begin{equation}
\alpha = \begin{bmatrix}
       \textrm{Na}  \\[0.3em]
       \textrm{Cl}   \\[0.3em]
       \textrm{U}   
     \end{bmatrix}
\end{equation}

Here we note that the order of species in the $\sigma$ and $\alpha$ vectors is arbitrary, but must be kept consistent
throughout the solution process.

Our stoichiometric matrix, $C$ is given by
\begin{equation}
C = \begin{bmatrix}
      1 & 1 & 0  \\[0.3em]
      0 & 3 & 1    
     \end{bmatrix}
\end{equation}

And the transpose is
\begin{equation}
C^T = \begin{bmatrix}
      1 & 0  \\[0.3em]
      1 & 3  \\[0.3em]
      0 & 1    
     \end{bmatrix}
\end{equation}

The vector of molecular mole fractions is
\begin{equation}
x_\sigma = \begin{bmatrix}
       0.25  \\[0.3em]
       0.75    
     \end{bmatrix}
\end{equation}

Evaluating the term $C^T x_\sigma$ yields
\begin{equation}
x_\alpha = 
\begin{bmatrix}
      1 & 0  \\[0.3em]
      1 & 3  \\[0.3em]
      0 & 1    
\end{bmatrix}
\begin{bmatrix}
       0.25  \\[0.3em]
       0.75    
     \end{bmatrix}
=
\begin{bmatrix}
       1 \times 0.25 + 0 \times 0.75 \\[0.3em]
       1 \times 0.25 + 3 \times 0.75 \\[0.3em]
       0 \times 0.25 + 1 \times 0.75    
     \end{bmatrix}
    =
\begin{bmatrix}
       0.25\\[0.3em]
       2.5 \\[0.3em]
       0.75    
     \end{bmatrix} 
\end{equation}

Then, the term $\sum_{k=1}^{N}{\left( C^T x_\sigma \right)}_k$ is calculated by summing up the terms in the
previous vector:
\begin{equation}
\sum_{k=1}^{N}{\left( C^T x_\sigma \right)}_k = {0.25 + 2.5 + 0.75} = 3.5
\end{equation}

Then, the atomic species mole fraction vector is calculated from Equation (\r ef{eq:xalpha}) as follows:
 
\begin{equation}
x_\alpha = \frac{1}{3.5} 
\begin{bmatrix}
       0.25\\[0.3em]
       2.5 \\[0.3em]
       0.75    
     \end{bmatrix} 
     =
     \begin{bmatrix}
       0.07142857 \\[0.3em]
       0.71428571 \\[0.3em]
       0.21428571
     \end{bmatrix} 
          =
     \begin{bmatrix}
       \frac{1}{14} \\[0.3em]
       \frac{10}{14} \\[0.3em]
       \frac{3}{14}\def\X#1{$#1$ &\tt\string#1}
     \end{bmatrix} 
\end{equation}

As a simple test we consider a box that contains molecules represented by symbols.  We let our container have 1 molecule of $\textrm{Na} \textrm{Cl}$ and 3 molecules of $\textrm{U} \textrm{Cl}_3$.  
\begin{equation}
\begin{bmatrix}
 \textrm{Na} \; \textrm{Cl} \\[0.3em]
 \textrm{U} \; \textrm{Cl} \; \textrm{Cl} \; \textrm{Cl} \\[0.3em]
 \textrm{U} \; \textrm{Cl} \; \textrm{Cl} \; \textrm{Cl} \\[0.3em]
  \textrm{U} \; \textrm{Cl} \; \textrm{Cl} \; \textrm{Cl}
\end{bmatrix}
\end{equation}

We clearly see that there are 14 atoms, 1,10 of which are chlorine, 1 is sodium, and 3 are uranium.  Thus, we have validated the solution
calculated above.


\subsubsection{Nuclidal Mole Fractions}

Continuing with our example, the isotopes of sodium to be considered are $\textrm{Na}^{23}$, which is  the only stable isotope.
   For chlorine, we have $\textrm{Cl}^{35}$ and $\textrm{Cl}^{37}$, which occur in nature with percentages of 75.78\% and 24.22\%,
   respectively.  For uranium naturally occuring isotopes are $\textrm{U}^{234}$, $\textrm{U}^{235}$, and $\textrm{U}^{238}$,
   with abundances of 0.0055\%, 0.72\%, and 99.2745\%, respectively.
   
   Thus, our nuclide matrix is given by
   
   \begin{equation}
  \nu =  \begin{bmatrix}
    1 & 0 & 0 \\[0.3em]
    0 & 0.7578 & 0 \\[0.3em]
   0 & 0.2422 & 0 \\[0.3em]
  0 & 0 & 0.000055 \\[0.3em]
  0 & 0 & 0.0072 \\[0.3em]
  0 & 0 & 0.992745 
\end{bmatrix}
\end{equation}

And calculation of the nuclidal mole fraction vector is given by
\begin{equation}
x_\nu = 
 \begin{bmatrix}
    1 & 0 & 0 \\[0.3em]
    0 & 0.7578 & 0 \\[0.3em]
   0 & 0.2422 & 0 \\[0.3em]
  0 & 0 & 0.000055 \\[0.3em]
  0 & 0 & 0.0072 \\[0.3em]
  0 & 0 & 0.992745 
\end{bmatrix}
     \begin{bmatrix}
       0.07142857 \\[0.3em]
       0.71428571 \\[0.3em]
       0.21428571
     \end{bmatrix} 
     =
      \begin{bmatrix}
   0.071429 \\[0.3em]
   0.54129 \\[0.3em]
   0.17300 \\[0.3em]
   0.000011786 \\[0.3em]
   0.0015429 \\[0.3em]
   0.21273
     \end{bmatrix} 
\end{equation}


To convert nuclidal mole fractions, $x_\nu$ to mass fractions, $y_\nu$, we simply utilize Equation (\ref{eq:x2y}),
where we have to have handy the vector of the nuclidal molecular weights.

To collapse the vector of nuclidal mole fractions back to molecular form, we sum up the mole fractions of each nuclide
to compute the atomic mole fraction.  However, we have to compute a new value of molecular weight of each atomic
mixture based on the formula given by Equation (\ref{eq:mmw}).

We consider a case in which the nuclidal vector is expanded to include $Cl^{36}$, so our vector contains 7 species.
Equations (\ref{eq:Wnu}) and (\ref{eq:summation}) are given as follows:
\begin{equation}
W_\nu =   \begin{bmatrix}
     22.99 & 0 & 0 & 0 & 0 & 0 & 0 \\[0.3em]
     0 & 34.97   & 35.97 & 	36.97 & 0 & 0 & 0  \\[0.3em]
     0 & 0 & 0 & 0 & 234.04  & 235.04  & 238.05 
   \end{bmatrix}
   \end{equation}
and
   \begin{equation}
S =   \begin{bmatrix}
    1 & 0 & 0 & 0 & 0 & 0 & 0 \\[0.3em]
     0 & 1   & 1 & 1 & 0 & 0 & 0  \\[0.3em]
     0 & 0 & 0 & 0 & 1 & 1 & 1 \\[0.3em]
   \end{bmatrix}
   \end{equation}

Let us assume our nuclidal mole fraction vector is given by 
\begin{equation}
 x_\nu =
   \begin{bmatrix}
   0.06189 \\[0.3em]
   0.44209 \\[0.3em]
   0.12378 \\[0.3em]
   0.15031 \\[0.3em]
   0 \\[0.3em]
   0.00088 \\[0.3em]
   0.22104
\end{bmatrix}
\end{equation}

In MATLAB, $\epsilon = 2.22 \times 10^{-16}$.  Then, from Eq.(\ref{eq:xnustar}),
\begin{equation}
 x_\nu^*=
   \begin{bmatrix}
   0.06189 \\[0.3em]
   0.44209 \\[0.3em]
   0.12378 \\[0.3em]
   0.15031 \\[0.3em]
   2.22 \times 10^{-16} \\[0.3em]
   0.00088 \\[0.3em]
   0.22104
\end{bmatrix}
\end{equation}


Therefore, from Eq.(\ref{eq:xalphafromxnu}), the atomic mole fractions are given by
\begin{boxeq}
  x_\alpha = 
  \begin{bmatrix}
    1 & 0 & 0 & 0 & 0 & 0 & 0 \\[0.3em]
     0 & 1   & 1 & 1 & 0 & 0 & 0  \\[0.3em]
     0 & 0 & 0 & 0 & 1 & 1 & 1 \\[0.3em]
   \end{bmatrix}
    \begin{bmatrix}
  0.06189 \\[0.3em]
   0.44209 \\[0.3em]
   0.12378 \\[0.3em]
  0.15031 \\[0.3em]
  2.22 \times 10^{-16} \\[0.3em]
  0.00088 \\[0.3em]
   0.22104
 \end{bmatrix}
=     
 \begin{bmatrix}
   0.06189 \\[0.3em]
   0.71618 \\[0.3em]
   0.22193 
\end{bmatrix}
\end{boxeq}

The molecular weight vector of atomic species is given by

\tiny{
\begin{boxeqarray}
W &=& \begin{bmatrix}
   \frac{1}{0.06189} & 0 & 0 \\[0.3em]
   0 & \frac{1}{0.71618} & 0 \\[0.3em]
   0 & 0 & \frac{1}{0.22193}
   \end{bmatrix}
   \begin{bmatrix}
    22.99 & 0 & 0 & 0 & 0 & 0 & 0 \\[0.3em]
     0 & 34.97   & 35.97 & 	36.97 & 0 & 0 & 0  \\[0.3em]
     0 & 0 & 0 & 0 & 234.04  & 235.04  & 238.05 
   \end{bmatrix}
    \begin{bmatrix}
 0.06189 \\[0.3em]
   0.44209 \\[0.3em]
   0.12378 \\[0.3em]
  0.15031 \\[0.3em]
  2.22 \times 10^{-16} \\[0.3em]
  0.00088 \\[0.3em]
   0.22104
 \end{bmatrix}  \nonumber  \\
 &=&   \begin{bmatrix}
  22.99 \\[0.3em]
   35.563 \\[0.3em]
   238.038 
\end{bmatrix}
\end{boxeqarray}
}





\end{document}




















{-#LANGUAGE DataKinds #-}
{-#LANGUAGE TypeOperators #-}
{-#LANGUAGE TypeFamilies #-}
module Main (main) where

import Data.List ( intercalate )
import Data.Monoid ( (<>) )

-- optparse-applicative
import Options.Applicative
-- parsec
import qualified Text.Parsec as P
-- units
import Data.Metrology.Poly ( LCSU(..) -- DefaultLCSU
                           , (%)
                           )
-- units-defs
import Data.Metrology.SI () -- DefaultLCSU instances
-- internal libraries
import Data.SERPENT.Types ( Material(..)
                          , MaterialComponent
                          , MaterialMeasure
                          )
import Data.SERPENT.Material ( materialComponents )
import Data.Units.Conversions ( scaleConcentrationFromSERPENT )
import Data.Units.Physical
import SERPENTParser
import Chart

data AppOptions = AppOptions
  { _fileName :: Maybe String
  , _csvFileName :: Maybe String
  , _graphFileName :: Maybe String
  , _addIsoFileName :: Maybe String
  }

parseAppOptions :: Parser AppOptions
parseAppOptions = AppOptions
  <$> optional ( argument str
     ( metavar "FILE"
    <> help "Data file to load" ) )
  <*> optional ( strOption
    ( short 'o'
   <> long "csv"
   <> metavar "CSVOUT"
   <> help "CSV output file" ) )
  <*> optional ( strOption
    ( short 'g'
   <> long "graph"
   <> metavar "GRAPHOUT"
   <> help "write graphs to file" ) )
  <*> optional ( strOption
    ( short 'a'
   <> long "add"
   <> metavar "ADDISO"
   <> help "Isotope data file to add density data to FILE" ) )

-- | Takes the program arguments and executes the parser(s) required
-- for requested execution.
-- TODO: Read from STDIN or a file. Only files are supported ATM.
runSerpentParser :: AppOptions -> IO ()
runSerpentParser (AppOptions Nothing _ _ _)
  = putStrLn "Reading from STDIN is not implemented (yet)."
runSerpentParser (AppOptions (Just fname) mofile grfile addfile)
  = do
      fileContents <- readFile fname
      let results = P.runParser processAllBlocks () fname fileContents
      case results of
        Left err -> print err
        Right val ->
            case addfile of
                Just f  -> do
                    addFileContents <- readFile f
                    let isoSoup = P.runParser materialComponents () f addFileContents
                    case isoSoup of
                        Left err -> print err
                        Right soup -> do
                            -- TODO: Combine the MaterialCompoents with the
                            -- existing Material
                            --let val' = addComponentsToMaterialBlocks val soup
                            let dummyMeasure = 1 % AtomicConcentration
                            let val' = [ Comment "Materials removed from the salt"
                                       , MaterialBlock $ Material "Removed" (Right dummyMeasure) soup
                                       ]
                            writeCSV mofile val'
                            writeGraphs grfile val'
                Nothing -> do
                    writeCSV mofile val
                    writeGraphs grfile val

{-
addComponentsToMaterialBlocks :: [SERPENTData] -> [MaterialComponent] -> [SERPENTData]
addComponentsToMaterialBlocks xs

addComponentToMaterialBlock :: SERPENTData -> [MaterialComponent] -> SERPENTData
addComponentToMaterialBlock c@(Comment _)      _ = c
addComponentToMaterialBlock s@(SetStatement _) _ = s
addComponentToMaterialBlock mb@(MaterialBlock (Material l fm mcs) mcs' = mb

mergeMaterialComponents :: [MaterialComponent] -> [MaterialComponent] -> [MaterialComponent]
mergeMaterialComponents mcs mcs' =
  where
    thisThatOrMergeMaterial (iso, m) (iso', m') = if nm == nm' then (iso, m :+: m')

-}

writeCSV :: Maybe String -> [SERPENTData] -> IO ()
writeCSV Nothing   _  = return ()
writeCSV _         [] = return ()
writeCSV (Just fn) xs = do
    let fullFileStrings = hdrString
                        ++ matStrings xs
                        ++ commStrings xs
                        ++ "\n"
    writeFile fn fullFileStrings
  where
    hdrString
      = "ZID,IsotopeName,ElementNumber,IsotopeNumber,ConcentrationUnit atoms/m^3\n"
    matStrings = concatMap  (\x -> case x of
                                     m@(MaterialBlock _) -> show m
                                     _                   -> "" )
    commStrings = concatMap (\x -> case x of
                                     c@(Comment _) -> show c
                                     _             -> "" )


writeGraphs :: Maybe String -> [SERPENTData] -> IO ()
writeGraphs Nothing   _  = return ()
writeGraphs _         [] = return ()
writeGraphs (Just fn) xs = mapM_ (writeGraph fn) mats
  where
    mats = filter (\m -> case m of MaterialBlock _ -> True; _ -> False) xs

main :: IO ()
main = execParser opts >>= runSerpentParser
  where
    opts = info (helper <*> parseAppOptions)
      ( fullDesc
     <> progDesc "Parse a SERPENT FILE"
     <> header "serpentsieve - a marshalling program for SERPENT data." )

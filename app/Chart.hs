{-#LANGUAGE DataKinds #-}
{-#LANGUAGE FlexibleContexts #-}
module Chart where

-- Radium
import Radium.Element ( symbol, atomicNumber )
-- units
import Data.Metrology.Poly ( numIn )
-- unts-defs
import Data.Metrology.SI.Mono ( ) -- DefaultLCSU instances
-- charts
import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo
-- base
import GHC.Exts ( groupWith )
import Data.Maybe ( mapMaybe )
-- internal
import SERPENTParser ( SERPENTData(..) )
import Data.SERPENT.Types ( Material(..), Isotope(..)
                          , MaterialComponent, MaterialMeasure
                          )
import Data.Units.Physical

--
groupData :: SERPENTData -> [[MaterialComponent]]
groupData (MaterialBlock (Material _ _ cs)) = groupWith groupByElement cs
  where
    -- MaterialComponet :: (Isotope, MaterialMeasure)
    groupByElement (Lost, _)             = 0
    groupByElement (MkIsotope el _ _, _) = atomicNumber el

groupData _ = [[]]


concentrationByElement :: [[MaterialComponent]] -> [(String,Int,Double)]
concentrationByElement [[]] = []
concentrationByElement css  = mapMaybe sumDensitiesByElement css

sumDensitiesByElement :: [MaterialComponent] -> Maybe (String,Int,Double)
sumDensitiesByElement [] = Nothing
sumDensitiesByElement ((Lost,m)            :xs) = Just ( "lost"
                                                       , 0
                                                       , concentration m
                                                       )
sumDensitiesByElement ((MkIsotope el _ _,m):xs) = Just ( symbol el
                                                       , atomicNumber el
                                                       , foldr (\(_,x) acc -> acc + concentration x) (concentration m) xs
                                                       )

concentration :: MaterialMeasure -> Double
concentration (Left m)  = m `numIn` MassDensity
concentration (Right m) = m `numIn` AtomicConcentration

writeGraph :: FilePath -> SERPENTData -> IO ()
writeGraph fn d = toFile def fn $ do
    layout_title .= "Density by Element"
    plot $ points "test data" just_numbers
  where
    values = concentrationByElement . groupData $ d
    x_labels = map (\(x,y,z) -> x) values
    x_domain = map (\(x,y,z) -> y) values
    just_numbers = map (\(x,y,z) -> (y,z)) values

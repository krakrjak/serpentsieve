{-#LANGUAGE FlexibleContexts,RankNTypes #-}
{-#LANGUAGE DataKinds #-}
{-#LANGUAGE TypeOperators #-}
{-#LANGUAGE TypeFamilies #-}
module SERPENTParser (module SERPENTParser) where

-- units
import Data.Metrology.Poly ( Three, Number(..), LCSU(..)
                           , (:/), (:^), (:@), (#)
                           , MkQu_ULN, MkQu_U
                           , sThree, showIn
                           )
import Data.Metrology.TH ( evalType )
-- units-def
import Data.Metrology.SI.Mono ( Gram(..), Meter(..), Centi(..) -- DefaultLCSU instances
                              )
-- Parsec
import qualified Text.Parsec as P
import Text.Parsec ((<?>), (<|>))
import Text.Parsec.String (Parser)
import Text.Parsec.Char (noneOf, endOfLine, alphaNum, spaces
                        ,digit, letter, anyChar)
-- internal imports
import Data.SERPENT.Types ( Material(..), MaterialComponent, MaterialMeasure )
import Data.SERPENT.Material ( materialComponents, elementLabel, fractionalMass )
import Data.Units.Physical

data SERPENTData = SetStatement String
                 | Comment String
                 | MaterialBlock Material

instance Show SERPENTData where
        show (SetStatement s) = "# Setting requested: " ++ show s ++ "\n"
        show (Comment s)      = "# Comment: " ++ show s ++ "\n"
        show (MaterialBlock (Material l m cs))
          = "mat " ++ show l ++ ",,,," ++ showMass m ++ "\n"
         ++ concatMap (\(iso, mass) -> show iso ++ "," ++ showMass mass ++ "\n") cs
         ++ "\n"

showMass :: MaterialMeasure -> String
showMass (Left am)  = show am
showMass (Right ac) = show ac

comments :: Parser SERPENTData
comments = P.try lineComment <|> P.try blockComment <?> "comments"

lineComment :: Parser SERPENTData
lineComment = do
        spaces
        comment <- P.char '%' *> P.many (noneOf "\r\n")
                   <?> "line comment"
        endOfLine
        spaces
        return $ Comment comment

blockComment :: Parser SERPENTData
blockComment = do
        spaces
        comment <- P.between  (P.string "/*") (P.string "*/") (P.many anyChar)
                   <?> "block comment"
        spaces
        return $ Comment comment

setLine :: Parser SERPENTData
setLine = do
        spaces
        line <- P.string "set" *> P.many (noneOf "\r\n")
                <?> "glogal setting"
        endOfLine
        spaces
        return $ SetStatement line

processAllBlocks :: Parser [SERPENTData]
processAllBlocks = P.manyTill (comments <|> P.try setLine <|> materialBlock) $ P.try P.eof

materialHeader :: Parser (String, MaterialMeasure)
materialHeader = do
        _ <- P.string "mat"
        label <- elementLabel
        totalDensity <- fractionalMass
        _ <- endOfLine
        return (label, totalDensity)

materialBlock :: Parser SERPENTData
materialBlock = do
        (label, totalDensity) <- spaces *> materialHeader
        components <- materialComponents
        return $ MaterialBlock $ Material label totalDensity components

cell :: Parser ()
cell = undefined

detector :: Parser ()
detector = undefined

include :: Parser ()
include = undefined

lattice :: Parser ()
lattice = undefined

pin :: Parser ()
pin = undefined

nest :: Parser ()
nest = undefined

particle :: Parser ()
particle = undefined

implicitfuelmodel :: Parser ()
implicitfuelmodel = undefined

explicitfuelmodel :: Parser ()
explicitfuelmodel = undefined

geometryplotter :: Parser ()
geometryplotter = undefined

surface :: Parser ()
surface = undefined

thermalscattering:: Parser ()
thermalscattering = undefined

transformation:: Parser ()
transformation = undefined

meshplotter :: Parser ()
meshplotter = undefined

history :: Parser ()
history = undefined

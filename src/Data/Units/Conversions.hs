{-#LANGUAGE TypeOperators #-}
{-#LANGUAGE TypeFamilies #-}
{-#LANGUAGE DataKinds #-}
module Data.Units.Conversions where

-- units
import Data.Metrology.Poly ( LCSU(..) -- DefaultLCSU
                           , (#), (%), (|*), (|/) 
                           )

-- units-defs
import Data.Metrology.SI () -- DefaultLCSU instances

-- local modules
import Data.Units.Physical

avogadroValue :: AtomicQu -> Double
avogadroValue m = mval * 6.022E23
  where
      mval = m # AtomicConcentration

atomicConcentration :: AtomicQu -> Double
atomicConcentration a = avo * (a # AtomicConcentration)
  where
    avo = avogadroValue (1 % AtomicConcentration)

{- | When specifying material amounts in atoms per volume, SERPENT scales
 - this value by a factor of 10^24/cm^3. This function takes a canonical
 - atoms per volume and divides it to scale for interoperation with SERPENT.
-}
scaleConcentrationToSERPENT :: AtomicQu -> AtomicQu
scaleConcentrationToSERPENT a = a |/ serpentScaleFactor

{- | When marshalling data from SERPENT files, it is nice to transform the
- data into natural measures. SERPENT uses a scaling factor of 10^24 for
- materials and isotopes specified using positive values.
-}
scaleConcentrationFromSERPENT :: AtomicQu -> AtomicQu
scaleConcentrationFromSERPENT a = a |* serpentScaleFactor

serpentScaleFactor = 10E24

scaleDensityFromSERPENT :: MassDQu -> MassDQu
scaleDensityFromSERPENT a = a |* serpentScaleFactor

scaleDensityToSERPENT :: MassDQu -> MassDQu
scaleDensityToSERPENT a = a |/ serpentScaleFactor

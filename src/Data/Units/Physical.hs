{-#LANGUAGE TypeOperators #-}
{-#LANGUAGE TypeFamilies #-}
{-#LANGUAGE DataKinds #-}
{-#LANGUAGE TemplateHaskell #-}
module Data.Units.Physical (module Data.Units.Physical) where

-- units
import Data.Metrology.TH   (declareDerivedUnit)
import Data.Metrology.Poly ( Three, Number, LCSU(..)
                           , (:/), (:^), (:@)
                           , MkQu_ULN
                           , DefaultUnitOfDim
                           )

-- units-defs
import Data.Dimensions.SI ( Length )
import Data.Units.SI ( Mole, Gram, Meter )
import Data.Units.SI.Prefixes ( Centi )

{-| The serpentsievedoc.tex file in Section 1.1 refers to the following
    physical values:
    Name                 Units
    Mass                 Gram         Data.Units.SI Gram
    Mole                 Mole         Data.Units.SI Mole
    Molecular Weight     Gram/Mol     this module   AtomicMass
    Mass Density         Gram/Volume  this module   MassDensity
    Molar Concentration  Mole/Volume  this module   MolarConcentration
    Atomic Concentration Atoms/Volume this module   AtomicConcentration
    Avogadro's Number    MoleInverse  Data.Units.Conversions AvogadroCount
    NOTE: use avogadroValue to extract the avogadro multiple of a given MoleInverse.
-}

declareDerivedUnit "AtomicMass"          
    [t| Gram :/ Mole |] 1 (Just "u") -- Universal Atomic Mass
declareDerivedUnit "MassDensity"
    [t| Gram :/ ((Centi :@ Meter) :^ Three) |]   1 (Just "ρ")
declareDerivedUnit "MolarConcentration"
    [t| Mole :/ ((Centi :@ Meter) :^ Three) |]   1 (Just "c")
declareDerivedUnit "AtomicConcentration"
    [t| Number :/ ((Centi :@ Meter) :^ Three) |] 1 (Just "č")

type AtomicMQu = MkQu_ULN AtomicMass          'DefaultLCSU Double
type MassDQu   = MkQu_ULN MassDensity         'DefaultLCSU Double
type MolarCQu  = MkQu_ULN MolarConcentration  'DefaultLCSU Double
type AtomicQu  = MkQu_ULN AtomicConcentration 'DefaultLCSU Double

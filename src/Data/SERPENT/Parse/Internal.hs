module Data.SERPENT.Parse.Internal (module Data.SERPENT.Parse.Internal) where
-- base
import Numeric (readSigned, readFloat)
-- parsec
import qualified Text.Parsec as P
import Text.ParserCombinators.Parsec.Prim (GenParser)
import Text.Parsec.Prim (parserZero)

doubleParse :: GenParser Char st Double
doubleParse = do
        s <- P.getInput
        case readSigned readFloat s of
            [(n, s')] -> n <$ P.setInput s'
            _         -> parserZero

{-#LANGUAGE DataKinds #-}
{-#LANGUAGE TypeOperators #-}
{-#LANGUAGE TypeFamilies #-}
module Data.SERPENT.Material ( materialComponent
                             , materialComponents
                             , elementLabel
                             , chemLibrary
                             , fractionalMass
                             ) where
-- Parsec
import qualified Text.Parsec as P
import Text.Parsec ((<?>), (<|>))
import Text.Parsec.String (Parser)
import Text.Parsec.Char ( endOfLine, alphaNum, spaces
                        , digit, letter )
import Text.Parsec.Combinator (notFollowedBy)
-- Radium
import Radium.Element (element, elementBySymbol)
-- units
import Data.Metrology ((%))
import Data.Metrology.Show ()
-- units-def
import Data.Metrology.SI.Mono ( Gram(..))
-- internal
import Data.SERPENT.Types ( Isotope(..)
                          , MaterialComponent
                          , MaterialLabel
                          , MaterialMeasure
                          )
import Data.SERPENT.Parse.Internal ( doubleParse )
import Data.Units.Conversions ( scaleConcentrationFromSERPENT, scaleDensityFromSERPENT )
import Data.Units.Physical

materialComponents :: Parser [MaterialComponent]
materialComponents = P.manyTill materialComponent (P.try (endOfLine *> endOfLine))
--FIXME: If a material component definition is the last line in the file,
--we barf.

materialComponent :: Parser MaterialComponent
materialComponent = P.try _lostComponent <|> P.try _materialComponentByName <|> _materialComponentByNumber
                <?> "material components"

_lostComponent :: Parser MaterialComponent
_lostComponent = P.label (do
        _ <- spaces *> P.string "lost"
        m <- fractionalMass
        return (Lost,m)
        ) "lost isotopes"

_materialComponentProperties :: Parser (String, MaterialMeasure)
_materialComponentProperties = do
        -- In the real world, the library information is missing sometimes...
        lib <- P.option "" chemLibrary
        m <- fractionalMass
        return (lib, m)

_materialComponentByName :: Parser MaterialComponent
_materialComponentByName = P.label (do
        elemName <- spaces *> P.many letter
        _ <- _dash
        isoParse <- P.many digit
        (libString, fracMass) <- _materialComponentProperties
        return (MkIsotope { chemelement=elementBySymbol elemName
                          , isonumber=read isoParse
                          , library=libString }
               ,fracMass)
       ) "material isotopes by name"

_materialComponentByNumber :: Parser MaterialComponent
_materialComponentByNumber = P.label (do
        elemParse <- spaces *> P.many digit
        -- We need to peek and see if we have a dot signifiying a library
        -- string. If not then we need to chop off a digit in elemParse
        maybeLib <- P.optionMaybe $ P.lookAhead _dot
        case maybeLib of
            Nothing ->do
                let libString = reverse . take 1 . reverse $ elemParse
                    isoParse = reverse . drop 1 . reverse $ elemParse
                    (elemNumber, isoNumber) = splitElemFromIso isoParse
                fracMass <- fractionalMass
                mkMyIso elemNumber isoNumber libString fracMass

            Just _ -> do
                let (elemNumber, isoNumber) = splitElemFromIso elemParse
                (libString, fracMass) <- _materialComponentProperties
                mkMyIso elemNumber isoNumber libString fracMass
       ) "material isotopes by number"
  where
    splitElemFromIso xs = ( read (reverse . drop 3 . reverse $ xs) :: Int
                          , read (reverse . take 3 . reverse $ xs) :: Int )
    mkMyIso e i l m = return ( MkIsotope { chemelement=element e
                                         , isonumber=i
                                         , library=l }
                             , m )


fractionalMass :: Parser MaterialMeasure
fractionalMass = P.label (do
                         val <- spaces *> doubleParse
                         notFollowedBy alphaNum
                         if val < 0
                         then return $ Left  $ scaleDensityFromSERPENT (val % MassDensity)
                         else return $ Right $ scaleConcentrationFromSERPENT (val % AtomicConcentration)
                         ) "material measure"

chemLibrary :: Parser String
chemLibrary = P.try _stringDigit <|> _dot *> P.many alphaNum

_stringDigit :: Parser String
_stringDigit = P.try (P.string "0")
           <|> P.try (P.string "1")
           <|> P.try (P.string "2")
           <|> P.try (P.string "3")
           <|> P.try (P.string "4")
           <|> P.try (P.string "5")
           <|> P.try (P.string "6")
           <|> P.try (P.string "7")
           <|> P.try (P.string "8")
           <|> P.string "9" -- Gotta fail sometime, we expect a friggin digit

elementLabel :: Parser MaterialLabel
elementLabel = spaces *> P.many (alphaNum <|> P.char '_' <|> P.char '-')
           <?> "element label in material block"

_dash :: Parser Char
_dash = P.char '-' <?> "expecing a dash -"

_dot :: Parser Char
_dot = P.char '.' <?> "expecting a dot ."


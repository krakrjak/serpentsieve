{-#LANGUAGE OverloadedStrings #-}
module Data.SERPENT.Types (module Data.SERPENT.Types) where

-- Radium
import Radium.Element (Element(..), atomicNumber, symbol)
-- units
import Data.Metrology.Show ()

-- internal
import Data.Units.Physical

type MaterialLabel = String
data Isotope = Lost | MkIsotope { chemelement :: Element
                                , isonumber :: Int
                                , library :: String
                                }

instance Show Isotope where
    show Lost                   = "lost,,,"
    show (MkIsotope el iso lib) = atomIDStr ++ ","
                                ++ isoNameStr ++ ","
                                ++ atomNumStr ++ ","
                                ++ isoNumStr
      where
        atomIDStr  = show $ (atomicNumber el * 1000) + iso
        atomNumStr = show $ atomicNumber el
        isoNumStr  = show iso
        metaStbTag = if _isDigit lib && (lib /= "0") then "m" ++ lib else ""
        isoNameStr = symbol el ++ "-" ++ isoNumStr ++ metaStbTag

type MaterialMeasure = Either MassDQu AtomicQu
type MaterialComponent = (Isotope, MaterialMeasure)
data Material = Material MaterialLabel MaterialMeasure [MaterialComponent]

_isDigit :: String -> Bool
_isDigit s | s == "0"  = True
           | s == "1"  = True
           | s == "2"  = True
           | s == "3"  = True
           | s == "4"  = True
           | s == "5"  = True
           | s == "6"  = True
           | s == "7"  = True
           | s == "8"  = True
           | s == "9"  = True
           | otherwise = False

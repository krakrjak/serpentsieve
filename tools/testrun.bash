#!/bin/bash

DATADIR=budata
OUTDIR=output

#There is no sense in continuing if the DATADIR does not exist
if [[ ! -d "${DATADIR}" ]]; then
  printf "${DATADIR} is not a directory, or is missing. Exiting!\n"
  exit 1
fi

#Unconditionally create the OUTDIR
mkdir -p "${OUTDIR}"

for dirOrFile in "${DATADIR}"/*; do
    if [[ -d "${dirOrFile}" ]]; then
        dir=${dirOrFile}
        printf "Processing files in the directory, %s\n" ${dir}
        for file in "${dir}"/*; do
            csvfile="${OUTDIR}"/${dir#*/}.$(basename ${file}).csv
            graphfile="${OUTDIR}"/${dir#*/}.$(basename ${file}).svg
            printf "Reading: %s |Writing: %s, %s\n" ${file} ${csvfile} ${graphfile}
            stack exec -- serpentsieve "${file}" -o "${csvfile}" -g "${graphfile}"
            sed -i -e 's/ m^-3$//' "${csvfile}"
        done
    elif [[ -f "${dirOrFile}" ]]; then
        file=${dirOrFile}
        csvfile="${OUTDIR}"/$(basename ${file}).csv
        graphfile="${OUTDIR}"/$(basename ${file}).svg
        printf "Reading: %s |Writing: %s, %s\n" ${file} ${csvfile} ${graphfile}
        stack exec -- serpentsieve "${file}" -o "${csvfile}" -g "${graphfile}"
        sed -i -e 's/ m^-3$//' "${csvfile}"
    else
        printf "Skipping \"${dirOrFile}\"\n"
    fi
done
